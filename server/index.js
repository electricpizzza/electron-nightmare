const express = require("express");
const app = express();
const bodyParser = require("body-parser");

const port = 3000;

var jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

app.get("/", (req, res) => {
  console.log(req.body, req.param, req.query);
  res.send("req.body, req.param, req.query");
});

app.post("/isdeleted", (req, res) => {
  const Nightmare = require("nightmare");
  const nightmare = Nightmare({ show: false });
  //   res.send(req.body);
  const url = req.body.url;
  nightmare
    .goto(url)
    .wait("#BodyContenCon h1")
    .evaluate(() => document.querySelector("#BodyContenCon h1").textContent)
    .end()
    .then((resp) => {
      res.send(resp === "Oups!");
    })
    .catch((error) => {
      console.error("Search failed:", error);
    });
});

app.listen(port, () => {
  console.log(`Example app listening on http://localhost:${port}`);
});
